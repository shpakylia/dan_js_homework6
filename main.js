/**
 * return array
 * @param array
 * @param withoutType
 */
function filterBy(array, withoutType) {
    let types = ['string', 'number', 'boolean', 'object', 'function', 'null', 'undefined'];
    let newArray = [];

    if(!array.length === 0) return array;
    if(!types.includes(withoutType)) return array;

    array.forEach(el=>{
        if(withoutType === 'null'){ // if null
            if(el !== null){
                newArray.push(el);
            }
        }else{
            if(typeof(el) !== withoutType){
                newArray.push(el);
            }
        }
    });

    return newArray;

}

let ar = ['hello', true, 'world', 23, '23', null, undefined, [1, 2]];
console.log(filterBy(ar, 'нн'));
console.log(filterBy(ar, 'string'));
console.log(filterBy(ar, 'null'));